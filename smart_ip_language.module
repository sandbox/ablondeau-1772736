<?php
/*
See the following page for some details on how this works
http://drupal.org/node/1497272
*/

/**
 * Implements hook_language_negotiation_info().
 */
function smart_ip_language_language_negotiation_info() {
  return array(
    'smart_ip' => array(
      'callbacks' => array(
        'language' => 'smart_ip_language_negotiation_from_ip', 
      ), 
      'file' => drupal_get_path('module', 'smart_ip_language') . '/smart_ip_language.module', 
      'weight' => -4, 
      'name' => t('Smart IP'), 
      'description' => t('Set language based on IP geo-location.'), 
      'cache' => 0,
    ),
  );
}

/**
 * Implements hook_language_negotiation_info_alter().
 */
function smart_ip_language_language_negotiation_info_alter(&$providers) {
  $providers['smart_ip']['callbacks']['language'] = 'smart_ip_language_negotiation_from_ip';
  $providers['smart_ip']['file'] = drupal_get_path('module', 'smart_ip_language') . '/smart_ip_language.module';
}

/**
 * Language selection callback function
 */
function  smart_ip_language_negotiation_from_ip($languages) {
  if(isset($_SESSION['smart_ip']['location']['country_code'])) {
    $lang_code = smart_ip_language_for_country($_SESSION['smart_ip']['location']['country_code']);
    if($lang_code) {
      
      //set $language too
      global $language;
      $language = $languages[$lang_code];
      
      return $lang_code;
    }
  }
  
  //if all else fails, return null so that other providers can have a say
  return null;  
}

/**
 * Hook into each language's form so that admins can specify which countries trigger the language.
 */
function smart_ip_language_form_alter(&$form, &$form_state, $form_id) {
  if($form_id == "locale_languages_edit_form") {
    $form['smart_ip_language_country_codes'] = array(
      '#type' => 'textfield',
      '#title' => t('Smart IP Country Codes'),
      '#description' => t('Enter a comma separated list of <a target="_blank" href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166 2-character country codes</a>.  Case doesn\'t matter.  This language will be triggered for visitors from the listed countries.'),
      '#default_value' => variable_get("smart_ip_language_country_codes_".arg(5), "")
    );
    $form['smart_ip_language_language_code'] = array(
      '#type' => 'hidden',
      '#value' => arg(5)
    );
    $form['#submit'][] = 'smart_ip_language_locale_languages_edit_form_submit';
  }
}

/**
 * Save changes to each language's settings
 */
function smart_ip_language_locale_languages_edit_form_submit($form, &$form_state) {
  if($form_state['values']['smart_ip_language_language_code']) {
    variable_set("smart_ip_language_country_codes_".$form_state['values']['smart_ip_language_language_code'], $form_state['values']['smart_ip_language_country_codes']);
  }
}

/**
 * Helps normalize country codes input by the user
 */
function smart_ip_language_trim_and_lower_country(&$country) {
  $country = strtolower(trim($country));
}

/**
 * Lookup which language should be used for the county
 */
function smart_ip_language_for_country($country_code) {
  if($country_code){
    $languages = language_list();
    foreach($languages as $code => $language) {
      if($countries_text = variable_get("smart_ip_language_country_codes_".$code,"")) {
        $countries = explode(",",$countries_text);
        array_walk($countries, 'smart_ip_language_trim_and_lower_country');
        if(in_array(strtolower($country_code), $countries)) {
          return $code;
        }
      }
    }
  }
}